/* eslint-disable import/no-commonjs */
/* eslint-disable import/no-extraneous-dependencies */
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const Webpack = require('webpack');

function buildConfig(env) {
  const config = {
    entry: './src/main.js',
    output: {
      path: `${env.dist}${env.basepath}`,
      publicPath: env.basepath,
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader',
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
          },
        },
        {
          test: /\.(ttf|eot|woff(2)?)(\?[a-z0-9=&.]+)?$/,
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]',
          },
        },
        {
          test: /\.ya?ml$/,
          loader: 'yaml-import-loader',
        },
        {
          test: /\.s?css$/,
          use: [
            'vue-style-loader',
            'css-hot-loader',
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },
        {
          enforce: 'pre',
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'eslint-loader',
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
        },
      ],
    },
    resolve: {
      alias: {
        vue$: 'vue/dist/vue.esm.js',
      },
    },
    plugins: [
      new VueLoaderPlugin(),
      new CopyWebpackPlugin(env.assets),
      new Webpack.DefinePlugin({
        'process.env.CONFIG': JSON.stringify(env),
      }),
    ],
  };

  return config;
}

module.exports = buildConfig;
